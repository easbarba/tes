#!/usr/bin/env bash

composer validate --strict
composer update

composer run-script fmt
composer run-script test
composer run-script bugs
composer run-script bugs++
