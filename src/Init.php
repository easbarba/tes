<?php

declare(strict_types=1);

/*
 * Tes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Tes. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Easbarba\Tes;

final class Init implements Base
{
    public function __construct(
        private string $destination,
        private bool $quiet = true,
        private bool $bare = false,
        private string $initialBranchName = 'master'
    ) {
    }

    public function finalCommand(): string
    {
        $result = Executable::NAME . "-C {$this->destination} init";
        $result .= " --initial-branch {$this->initialBranchName}";

        if ($this->quiet === false) {
            $result .= ' --quiet';
        }

        if ($this->bare === true) {
            $result .= ' --bare';
        }

        return $result;
    }

    public function exec(): void
    {
        $this->createDirectory();

        system($this->finalCommand());
    }

    /*
     * create directory in destination
     */
    private function createDirectory(): void
    {
        if (is_dir($this->destination) === false) {
            mkdir($this->destination);
        }
    }
}
