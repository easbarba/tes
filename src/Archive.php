<?php

declare(strict_types=1);

/*
 * Tes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Tes. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Easbarba\Tes;

final class Archive implements Base
{
    public function __construct(
        private string $destination,
        public string $format = 'tar.gz'
    ) {
    }

    public function finalCommand(): string
    {
        $result = Executable::NAME . " archive {$this->destination}";
        $result .= " --format {$this->format}";

        return $result;
    }

    public function exec(): void
    {
        system($this->finalCommand());
    }
}
