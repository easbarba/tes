<?php

declare(strict_types=1);

/*
 * Tes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Tes. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Easbarba\Tes;

final class Klone implements Base
{
    public function __construct(
        private Repository $repository,
        private string $destination,
        private int $depth = -1,
        private bool $quiet = false,
        private bool $singleBranch = false
    ) {
    }

    public function finalCommand(): string
    {
        $result = Executable::NAME . ' clone ';

        if ($this->singleBranch === true) {
            $result .= '--single-branch ';
        }

        if ($this->depth !== -1) {
            $result .= "--depth {$this->depth} ";
        }

        if ($this->quiet === true) {
            $result .= '--quiet ';
        }

        return $result . "{$this->repository->url} {$this->destination}";
    }

    public function exec(): void
    {
        system($this->finalCommand());
    }
}
