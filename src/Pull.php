<?php

declare(strict_types=1);

/*
 * Tes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Tes. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Easbarba\Tes;

final class Pull implements Base
{
    public function __construct(
        private string $destination,
        private bool $quiet = false,
        private int $depth = -1
    ) {
    }

    public function plainOpen(string $destination): string
    {
        $destination = realpath($destination);
        if ($destination === false) {
            throw new \Exception('Folder does not exist!');
        }

        $destinationGitFolder =
            $destination .
            DIRECTORY_SEPARATOR .
            '.git' .
            DIRECTORY_SEPARATOR .
            'config';
        if (! file_exists($destinationGitFolder)) {
            throw new \Exception('Folder is not a repository');
        }

        return $destination;
    }

    public function exec(): void
    {
        system($this->finalCommand());
    }

    public function finalCommand(): string
    {
        $result = Executable::NAME . " -C {$this->destination} pull";

        if ($this->quiet === true) {
            return $result .= ' --quiet';
        }

        if ($this->depth !== -1) {
            $result .= $this->depth;
        }

        return $result;
    }
}
