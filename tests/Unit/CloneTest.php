<?php

declare(strict_types=1);

/*
 * Tes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Tes. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Tests\Unit;

use Easbarba\Tes\Klone;
use Easbarba\Tes\Repository;

beforeEach(function () {
    $this->repository = new Repository(name: "guzzle", url: "https://github.com/guzzle/guzzle");
    $this->clone = new Klone(repository: $this->repository, destination: "/foo/bar/baz");
});

it('has basic final command correctly', function () {
   $found = $this->clone->finalCommand();
   $expected = 'git clone https://github.com/guzzle/guzzle /foo/bar/baz';

   expect($found)->toBe($expected);
});

it('has full final command correctly', function () {
    $clone = new Klone(repository: $this->repository,
                       depth: 2,
                       destination: "/foo/bar/baz",
                       quiet: false,
                       singleBranch: true);
    $found = $clone->finalCommand();
    $expected = 'git clone --single-branch --depth 2 https://github.com/guzzle/guzzle /foo/bar/baz';

    expect($found)->toBe($expected);
});
