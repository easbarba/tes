<?php

declare(strict_types=1);

/*
 * Tes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Tes. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Tests\Unit;

use Easbarba\Tes\Archive;

beforeEach(function () {
    $this->archive = new Archive(destination: "/foo/bar/baz");
});

it('has basic final command correctly', function () {
   $found = $this->archive->finalCommand();
   $expected = 'git archive /foo/bar/baz --format tar.gz';

   expect($found)->toBe($expected);
});

it('has full final command correctly', function () {
    $archive = new Archive(destination: "/foo/bar/baz", format: 'zip');
    $found = $archive->finalCommand();
    $expected = 'git archive /foo/bar/baz --format zip';

    expect($found)->toBe($expected);
});
