<?php

declare(strict_types=1);

/*
 * Tes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Tes. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Tests\Unit;

use Easbarba\Git\CloneOptions;

beforeEach(function () {

});

// it('has peaceful plain clone parameters', function () {
//     $expected = $this->git->plainClone(
//         '/to/there',
//         new CloneOptions(
//             url: 'https://github.com/github/gitignore',
//             branch: '--single-branch',
//             depth: 2,
//             progress: false
//         )
//     );
//     $actual =
//         'git clone --single-branch --depth 2 --quiet https://github.com/github/gitignore /to/there';

//     $this->assertEquals($expected, $actual);
// });

// it('has noisy plain clone parameters', function () {
//     $expected = $this->git->plainClone(
//         '/to/here',
//         new CloneOptions(
//             url: 'https://github.com/github/gitignore',
//             branch: '--single-branch',
//             depth: 3,
//             progress: true
//         )
//     );
//     $actual =
//         'git clone --single-branch --depth 3 --verbose https://github.com/github/gitignore /to/here';

//     $this->assertEquals($expected, $actual);
// });

// it('does not pull if it does not exist', function () {
//     $open = $this->git->plainOpen('/to/here');
//     $expected = false;
//     $actual = $this->git->pullCommand($open, false);

//     $this->assertEquals($expected, $actual);
// });

// it('pulls repository', function () {
//     $open = \Easbarba\Tes\Git\Utils::plainOpen(configsFolder());
//     var_dump($open);

//     $expected = 'git -C '.configsFolder().' pull --quiet';
//     $actual = $this->_git->pull($open, false);

//     $this->assertEquals($expected, $actual);
// });
