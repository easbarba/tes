<?php

declare(strict_types=1);

/*
 * Tes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Tes. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Tests\Unit;

use Easbarba\Tes\Init;

beforeEach(function () {
    $this->init = new Init(destination: "/foo/bar/baz");
});

it('has basic final command correctly', function () {
    $found = $this->init->finalCommand();
    $expected = 'git-C /foo/bar/baz init --initial-branch master';

    expect($found)->toBe($expected);
});

it('has initial branch name correctly', function () {
    $init = new Init(destination: "/foo/bar/baz", initialBranchName: 'main');

    $found = $init->finalCommand();
    $expected = 'git-C /foo/bar/baz init --initial-branch main';

    expect($found)->toBe($expected);
});

it('inits quietly', function () {
    $init = new Init(destination: "/foo/bar/baz", quiet: false);

    $found = $init->finalCommand();
    $expected = 'git-C /foo/bar/baz init --initial-branch master --quiet';

    expect($found)->toBe($expected);
});

it('bares new repository', function () {
    $init = new Init(destination: "/foo/bar/baz", bare: true);

    $found = $init->finalCommand();
    $expected = 'git-C /foo/bar/baz init --initial-branch master --bare';

    expect($found)->toBe($expected);
});

it('has all set options correctly', function () {
    $init = new Init(destination: "/foo/bar/baz",
                     initialBranchName: 'main',
                     bare: true,
                     quiet: false);

    $found = $init->finalCommand();
    $expected = 'git-C /foo/bar/baz init --initial-branch main --quiet --bare';

    expect($found)->toBe($expected);
});
