# Tes is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Tes is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Tes. If not, see <https://www.gnu.org/licenses/>.

# DEPS: GNU Bash, Gawk, fzf

.DEFAULT_GOAL := tests

RUNNER ?= podman
COMPOSER_IMAGE := composer:lts

NAME := tes
VERSION := $(shell gawk '/VERSION/ {version=substr($$4,2,5); print version};' ./src/Information.php )

commands:
	${RUNNER} run --rm -it \
		--volume ${PWD}:/app:Z \
		--workdir /app \
		${COMPOSER_IMAGE} bash -c '$(shell cat ./commands | fzf)'

install:
	composer install --prefer-dist --no-progress

phar:
	box compile

gpg:
	gpg -u ${EMAIL} --detach-sign --output ./bin/${NAME}.phar.asc ./bin/${NAME}.phar

img:
	${RUNNER} build --file ./Dockerfile --tag ${USER}/${NAME}:${VERSION}
	${RUNNER} run --rm ${USER}/${NAME}:${VERSION}

.PHONY: commands img gpg phar install
