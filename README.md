<!--
Tes is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Tes is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Tes. If not, see <https://www.gnu.org/licenses/>.
-->

# Tes

Yet another git wrapper, simply manage git repository in a all-in php interface,
based off on [gitPython](gitpython.readthedocs.io).

# Commands

| Command    | implemented |
|------------|-------------|
| clone      | mostly      |
| pull       | mostly      |
| repository | mostly      |

# Installation

Install via **composer**: `composer require easbarba/tes`.

## [Documentation](docs)

Check usage on [documentation](docs).

## GNU Guix

In a system with GNU Guix binary installed, its even easier to grab all
dependencies: `guix shell`.

## LICENSE

[GNU GENERAL PUBLIC LICENSE Version 3](https://www.gnu.org/licenses/gpl-3.0.en.html)

