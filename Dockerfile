FROM composer:lts as builder
WORKDIR /app
COPY composer.json composer.lock ./
RUN composer validate --strict
RUN composer install --prefer-dist --no-progress

FROM php:8-alpine
MAINTAINER EAS Barbosa <easbarba@outlook.com>
RUN apk add --update git
WORKDIR /app
COPY --from=builder /app/vendor /app/vendor
COPY . .
CMD ["./vendor/bin/pest", "--parallel", "--compact"]
