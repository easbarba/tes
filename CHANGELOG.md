<!--
Tes is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Tes is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Tes. If not, see <https://www.gnu.org/licenses/>.
-->

# CHANGELOG

## 0.3.3
 - add license header to possible all files

## 0.3.2
 - bump dependencies
 - chore: improve project development toolings

## 0.3.1

- fix: pull correctly adjoin quiet option

## 0.3.0

- add init command
- pest 2

## 0.2.0

- Add initial ...

## 0.1.0

- initial structure
